<?php

    include("PHP/fonctions/commun.php");
    include("PHP/includes/connexion.php");

    $placeHolderPseudonymeConnect = "placeholder='Pseudonyme'";

    $placeHolderFirstname = "placeholder='Nom'";
    $placeHolderLastname = "placeholder='Prénom'";
    $placeHolderEmail = "placeholder='Email'";
    $placeHolderEmailConfirm = "placeholder='Email *'";
    $placeHolderPseudonyme = "placeholder='Pseudonyme'";
    $errorMsg = NULL;

    if (isset($_POST['firstname'])) {
        $placeHolderFirstname = "value='".htmlspecialchars($_POST['firstname'])."'";
    }
    if (isset($_POST['lastname'])) {
        $placeHolderLastname = "value='".htmlspecialchars($_POST['lastname'])."'";
    }
    if (isset($_POST['email'])) {
        $placeHolderEmail = "value='".htmlspecialchars($_POST['email'])."'";
    }
    if (isset($_POST['email_confirm'])) {
        $placeHolderEmailConfirm = "value='".htmlspecialchars($_POST['email_confirm'])."'";
    }
    if (isset($_POST['pseudonyme'])) {
        $placeHolderPseudonyme = "value='".htmlspecialchars($_POST['pseudonyme'])."'";
    }
    if (isset($_POST['pseudonymeConnect'])) {
        $placeHolderPseudonymeConnect = "value='".htmlspecialchars($_POST['pseudonymeConnect'])."'";
    }

    if(isset($_POST['pseudonyme'])&& isset($_POST['email_confirm']) && isset($_POST['email']) && isset($_POST['lastname']) && isset($_POST['firstname'])) {

       $errorMsg = inscription($_POST);
    }

    if (isset($_POST['pseudonymeConnect'])) {
        $_SESSION['pseudonyme'] = $_POST['pseudonymeConnect'];
        //setcookie("pseudonyme", $_POST['pseudonymeConnect'], time()+365*24*3600,"/");
    } elseif (isset($_POST['pseudonyme'])) {
        $_SESSION['pseudonyme'] = $_POST['pseudonyme'];
        //setcookie("pseudonyme", $_POST['pseudonyme'], time()+365*24*3600,"/");
    }


?>
<!DOCTYPE html>
<html lang="fr">
<head>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
</head>
<body>


  <div class="wrapper">
    <div class="left side">
      <div class="inner-left">

            <?php
                if(isset($errorConnect)) {
                    echo "<ul class='errorMsg' >";
                    foreach($errorConnect as $value => $data) {
                        if(!empty($data)) {
                            echo "<li>".$data."</li>";
                        }
                    }
                    echo "</ul>";
                }
            ?>

        <form action="index.php" method="POST" class="forms" >
          <ul>
            <li>
              <div class="form-span unit">
                <div class="input">
                  <label class="icon-right">
                    <i class="fa fa-at"></i>
                  </label>
                  <input id="pseudoConnect" type="text" name="pseudonymeConnect" <?php echo $placeHolderPseudonymeConnect ?> maxlength="15" required>
                </div>
              </div>
            </li>

            <li>
              <div class="form-span unit">
                <div class="input">
                  <label class="icon-right" >
                    <i class="fa fa-key"></i>
                  </label>
                  <input id="passwordConnect" type="password" name="passwordConnect" onpaste='return false' placeholder="Mot de passe" maxlength="25" required>
                </div>
              </div>
            </li>
            <li>
                <button class="form-button" type="submit" name="submit">Longue vie et prospérité <i class="fa fa-hand-spock-o" aria-hidden="true"></i></button>
              </li>
                <li><button class="form-button form-button-demo" onclick="demonstration()">Démo</button>
            </li>
          </ul>
        </form>
      </div>
    </div>

    <div class="position-logo">
      <img src="img/logo-index.png" >
    </div>

    <div class="right side">
      <div class="inner-right">

            <?php
                if(isset($errorMsg)) {
                    echo "<ul class='errorMsg'>";
                    foreach($errorMsg as $value => $data) {
                        if(!empty($data)) {
                            echo "<li>".$data."</li>";
                        }
                    }
                    echo "</ul>";
                }
            ?>
        <form action="index.php" method="POST" class="forms">
          <ul>
            <li>
              <div class="form-content">
                <div class="form-row">
                  <div class="form-span unit">
                    <div class="input">
                      <label class="icon-right" >
                        <i class="fa fa-user-o"></i>
                      </label>

                      <input type="text" id="firstname" name="firstname" <?php echo $placeHolderFirstname ?> maxlength="15" required>

                    </div>
                  </div>
                  </div>
                </div>
                </li>

                <li>
                  <div class="form-span unit">
                    <div class="input">
                      <label class="icon-right">
                        <i class="fa fa-user"></i>
                      </label>

                      <input type="text" id="lastname" name="lastname" <?php echo $placeHolderLastname ?> maxlength="15" required>

                    </div>
                  </div>
                </li>

                <li>
                  <div class="form-span unit">
                    <div class="input">
                      <label class="icon-right">
                        <i class="fa fa-envelope-o"></i>
                      </label>

                      <input type="email" name="email" <?php echo $placeHolderEmail ?> maxlength="30" required>

                    </div>
                  </div>
                </li>

                <li>
                  <div class="form-span unit">
                    <div class="input">
                      <label class="icon-right">
                        <i class="fa fa-paper-plane"></i>
                      </label>

                      <input type="email" name="email_confirm" <?php echo $placeHolderEmailConfirm ?> maxlength="30" required>

                    </div>
                  </div>
                </li>

                <li>
                  <div class="form-span unit">
                    <div class="input">
                      <label class="icon-right">
                        <i class="fa fa-key"></i>
                      </label>
                      <input type="password" name="password" onpaste='return false' placeholder="Mot de passe" maxlength="15" required>
                    </div>
                  </div>
                </li>

                <li>
                  <div class="form-span unit">
                    <div class="input">
                      <label class="icon-right">
                        <i class="fa fa-key"></i>
                      </label>
                      <input type="password" name="password_confirm" onpaste='return false' placeholder="Mot de passe *" maxlength="15" required>
                    </div>
                  </div>
                </li>

                <li>
                  <div class="form-span unit">
                    <div class="input">
                      <label class="icon-right">
                        <i class="fa fa-at"></i>
                      </label>

                      <input type="text" name="pseudonyme" <?php echo $placeHolderPseudonyme ?> maxlength="15" required>

                    </div>
                  </div>
                </li>
                <li>
                  <div class="form-span unit">
                    <div class="input">
                      <button class="form-button" type="submit" name="submit">Rejoignez le réseau <i class="fa fa-smile-o" aria-hidden="true"></i></button>
                    </div>
                  </div>
                </li>
              </ul>


            <div>
              <input name="CGU" id="checkbox2" type="checkbox" required>
              <label for="checkbox2">Acceptez les <a href="CGU.html" >conditions générales d'utilisation</a></label>
            </div>
            <div class="geek">
              <p>Êtes-vous un(e) geek ?</p>
              <input value="oui" name="geek" type="radio" id="geekoui" checked >
              <label for="geekoui">Oui |</label>
              <input value="non" name="geek" type="radio" id="geeknon">
              <label for="geeknon">Non</label>
				</div>
            </form>

          </div>
        </div>
        </div>
      </body>
</html>
<script type="text/javascript">
    function demonstration(){
        document.getElementById("pseudoConnect").value = "gandalf";
        document.getElementById("passwordConnect").value = "test";
    }

</script>
