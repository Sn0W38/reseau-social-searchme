# GET/POST

Note d'utilisation des appels GET et POST sur le site SearchMe.

## Principes d'utilisation

De maniére général, les appels du POST se feront dans le cas des traitement des formulaire d'inscription, modification de profils.
Les appels du GET se feront dans le cas de recherches de profils.

## POST

Les valeurs sont traitées par les fonctions dataEcriture() et dataLecture(), et sont sauvegardées dans le dossier /UserData.

### Formulaire d'inscription

* Pseudonyme
* Nom
* Prénom
* Adresse e-mail
* Mot de passe (cryptage md5)
* Etes vous un geek?
* Conditions générales

### Formulaire de connexion

* Pseudonyme
* Mot de passe

### Modification du profil

* Pseudonyme
* Mot de passe
* Image de profil
* Sexe
* Age
* Languages de programmation

## GET

### Affichage d'un profil spécifique

Les profils sont générés à partir du pseudo :

    searchme.fr/profils?pseudonyme=BillGates

Génére le profil du fichier UserData/BillGates.txt

### Affichage de son profil (avec possibilité de le modifier)

Lors des la génération du profil, ont vérifie si $_GET['pseudo'] correspond au cookie $_SESSION['pseudo'] définie au moment de la connexion.
Si les valeurs sont les mêmes, alors l'utilisateur peut accéder à la modification de son profil.
