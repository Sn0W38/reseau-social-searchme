# Fonctions d'écriture et de lectures.
## *note technique*
### Read-write.php

#### dataEcriture($data, $rewrite) :
    dataEcriture         : encode en json et ecrit dans un fichier txt. Crée le fichier si celui ci n'existe pas. 
    @param    $data      : données utilisateurs
    @param    $rewrite   : "rewrite", écrase le contenu.
    @return   $fileExist : retourne true ou false
    
#### dataLecture($filename) :
    dataLecture           : lis dans un fichier txt, decode le json et retourne un tableau 
    @param    $filename   : fichier dans lequel on récupére les données
    @return   $data       : retourne une variable contenant le contenu du fichier txt sous forme de tableau   
    
*Table4* 