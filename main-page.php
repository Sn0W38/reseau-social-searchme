<!DOCTYPE html>
<?php
if(isset($_POST["test_modif"])){$nb_msg = $_POST["tes"];}
if(!isset($_GET["page"])){
    $numPage = 1;
    $positionCommentaire = 1;
}else{
    $numPage = $_GET["page"];
    $positionCommentaire = ($_GET["page"]);
}
include("PHP/fonctions/commun.php");
$limitCommentaire = 10;
$dataPseudonyme = NULL;
//if(!isset($page_test)){$tesa = true;}
if (isset($_SESSION['pseudonyme'])) {
    modifProfil($_SESSION['pseudonyme']);
    $dataPseudonyme = $_SESSION['pseudonyme'];
}
//require_once("PHP/includes/genere-short-profil.php");
require_once('PHP/includes/genere-profil.php');

if (empty($_SESSION) OR $_SESSION == NULL) {
  header ('Location: index.php');
  exit();
} elseif (empty($_GET['pseudonyme']) || $profilNotFoundError) {
    header ('Location: main-page.php?pseudonyme='.$_GET["pseudonyme"].'&page='.$_GET["page"]."&connect=on");
    exit();
}
    if(isset($_SESSION["pseudonyme"])) {
        if($_SESSION["pseudonyme"] == "gandalf") {
            $inputAction = "disabled";
        }
    }

//Declaration variables
//var_dump($data);
//var_dump($_POST);

?>

<html lang="fr">

<head>
	<title>$SearchMe</title>
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/ecraseStyle.css">
  <meta charset="utf-8">
</head>

<body>
	<header>
		<div
            <?php
              if (isset($_GET['connect'])){
                if ($_GET['connect'] == "on"){
                  echo "class='position-logo animated animatedFadeInUp fadeInUp'";
                }else{
                  echo "class='position-logo'";
                }

              }else{
                echo "class='position-logo'";
              }
            ?> >
        </div>
	</header>

	<main class="container">
		<section class="profil">
			<aside>

				<form class="form-profil">
					<p class="head"><?php echo $pseudonyme ?></p>
					<p class="head"><i class="fa fa-hashtag" aria-hidden="true"></i><?php echo $pseudonyme ?></p>
          <img class="picture-profil" src="<?php echo $img ?>">

					<div class="tree">

            <?php include("PHP/includes/hidden-icon.php"); ?>

						<div>
							<p class="hidden-icon1 hiddentest"><i class="fa fa-hashtag" aria-hidden="true"></i> <?php echo $pseudonyme ?><p>
							<p class="hidden-icon2 hiddentest"><i class="fa fa-quote-right" aria-hidden="true"></i> <?php echo $about ?></p>
							<p class="hidden-icon3"><i class="fa fa-calendar" aria-hidden="true"></i> Inscrit le : <?php echo $register ?></p>
							<p class="hidden-icon4"><i class="fa fa-birthday-cake" aria-hidden="true"></i> <?php echo $birthday ?></p>
							<p class="hidden-icon5"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $adress ?></p>
							<p class="hidden-icon6"><i class="fa fa-envelope-o" aria-hidden="true"></i> <?php echo $email ?></p>
							<p class="hidden-icon7"><i class="fa fa-linux" aria-hidden="true"></i> <?php echo $distrib ?></p>
						</div>

						</div>

						<div class="hidden-icon8 hidden-tree">
							<input id="n-1" type="checkbox">
							<label class="leftlabel" for="n-1">Site web :</label>
                            <div class="sub">
								<a class="hidden-icon9" target="_blank" href="http://<?php echo $site1 ?>"><?php echo $site1 ?></a>
								<a class="hidden-icon10" target="_blank" href="http://<?php echo $site2 ?>"><?php echo $site2 ?></a>
							</div>
						</div>

						<div class="hidden-icon11 hidden-tree">
							<input id="n-2" type="checkbox">
							<label class="leftlabel" for="n-2">Langages</label>
							<div class="sub">
								<p class="hidden-icon12"><?php echo $language1 ?></p>
								<p class="hidden-icon13"><?php echo $language2 ?></p>
								<p class="hidden-icon14"><?php echo $language3 ?></p>
							</div>
						</div>

						<div class="hidden-icon15 hidden-tree">
							<input id="n-3" type="checkbox">
							<label class="leftlabel" for="n-3">Hobbies</label>
							<div class="sub">
								<p class="hidden-icon16"><?php echo $hobbie1 ?></p>
								<p class="hidden-icon17"><?php echo $hobbie2 ?></p>
								<p class="hidden-icon18"><?php echo $hobbie3 ?></p>
							</div>
						</div>

					</form>
				</aside>
			</section>

			<section class="wall">
				<aside style="display: flex; flex-flow: row wrap;">
            <form style="box-shadow: none; padding-top: 40px;" class="test" method="post" action="">
                <p class="head">Mur</p>
                <?php
                    include("PHP/includes/livre-dor.php");
                    $pageTotal = dataLectureLivre($positionCommentaire, $limitCommentaire);

                    /*if (isset($_GET["page"])) {
                        $page = $_GET["page"];
                    } else {
                        $page = 1;
                    }
                    limitCom(10, $page, $filename);*/
                ?>
                <input type="hidden" name="test_msg" value="1">

            </form>
            <form id="paginationForm" class="boxShadowNone" method="GET" action="">

                <div class="pagination">
                    <input class="buttonPagination" type="submit" name="page" value="<?php echo ($numPage-1) ?>"
                    <?php
                        if(($numPage-1) < 1 ){
                            echo "disabled";
                        }
                    ?>
                    >
                    <input class="buttonPagination" id="actualPageButton" type="submit" name="page" value="<?php echo ($numPage) ?>">
                    <input class="buttonPagination" type="submit" name="page" value="<?php echo ($numPage+1) ?>"
                    <?php
                        if(($numPage+1) > $pageTotal ){
                            echo "disabled";
                        }
                    ?>
                    >
                    <input type="hidden" name="pseudonyme" value="<?php echo $_GET["pseudonyme"] ?>">
                </div>

            </form>
          <form class="form-wall boxShadowNone" method="post" action="main-page.php?pseudonyme=<?php if(isset($_GET['pseudonyme'])){echo $_GET['pseudonyme'];} ?>">

            <ul class="commentSubmit">
                <li>
                    <textarea maxlength="256" required name="message"
                              <?php
                                    if(!isset($_POST["test_msg"])) {

                                            echo "placeholder='Insérez votre commentaire ici ...'";

                                    }
                                ?> ><?php
                                    if(isset($_POST["test_msg"])) {
                                        if($_POST["test_msg"]) {
                                            echo $page["commentaire".($_POST["tes"])];
                                        }
                                    }
                                ?></textarea>
                </li>
                <li>
                    <?php if(isset($_POST["test_msg"])){if($_POST["test_msg"]){echo "<input type='hidden' name='test_modif' value='0'>";}} ?>
                    <?php if(isset($_POST["test_msg"])){if($_POST["test_msg"]){echo "<input type='hidden' name='tes' value='".$_POST["tes"]."'>";}} ?>
                    <button type="submit"><i class="fa fa-commenting" aria-hidden="true"></i></button>
                    <input type="hidden" name="pseudonyme" value="<?php echo $_GET["pseudonyme"] ?>">

                </li>
            </ul>
					</form>

				</aside>
			</section>

			<div class="flex-right">

				<section class="list">
					<aside>

                        <form enctype='multipart/form-data' action='' method='post' class="form-profil-second">

							                  <p class="head"><?php echo $pseudonymeConnect ?></p>

                                <div class="wrap-figure">
                                <figure>
                                <input type='hidden' name='MAX_FILE_SIZE' value='2097152'>
                                <input <?php echo $inputAction; ?> id="test" name='img' type='file' onchange="this.form.submit();">
                                <label for="test" class="pictureProfilForm" style="background: url(<?php echo $imgConnect ?>); background-position: center; background-repeat: no-repeat; background-size:contain; padding:0;"><figcaption>Changer d'image</figcaption></label>
                                </figure>
                                </div>

                                <a href="#contact-us"><i class="fa fa-cog" aria-hidden="true"></i> Modifier profil</a>
                                <a href="PHP/fonctions/deconnexion.php"><i class="fa fa-sign-out" aria-hidden="true"></i> Se déconnecter</a>

                                <form action="" method="post">

                                <textarea <?php echo $inputAction; ?> type="text" name="about" placeholder="Quoi de neuf?" maxlength="140"><?php echo $aboutConnect ?></textarea>
                                <button type="submit"><i class="fa fa-commenting" aria-hidden="true"></i></button>

                              </form>

						</form>

					</aside>
				</section>

                        <?php
                            if($_GET["pseudonyme"] == $_SESSION["pseudonyme"]) {
                                include("PHP/includes/template-choice.php");
                            }
                        ?>

					<section class="list">
						<aside class="test13">
              <form class="form-list" method="POST">
                <p class="head-second">Liste des membres</p>
                <div class="gamepadbouton">
                  <input name="geekfilter" type="radio" value="3" onClick="this.form.submit();" <?php if (isset($_POST['geekfilter'])) { filtrecheck($_POST['geekfilter'], 3); } else { echo "checked"; }; ?>>
                  <label for="original" class="rightlabel"><i class="fa fa-gamepad colorGamepad1" aria-hidden="true"></i></label>

                  <input name="geekfilter" type="radio" value="1" onClick="this.form.submit();" <?php if (isset($_POST['geekfilter'])) { filtrecheck($_POST['geekfilter'], 1); }?>>
                  <label for="original" class="rightlabel"><i class="fa fa-gamepad colorGamepad2" aria-hidden="true"></i></label>

                  <input name="geekfilter" type="radio" value="0" onClick="this.form.submit();" <?php if (isset($_POST['geekfilter'])) { filtrecheck($_POST['geekfilter'], 0); }?>>
                  <label for="original" class="rightlabel"><i class="fa fa-gamepad colorGamepad3" aria-hidden="true"></i></label>
                </div>

                <div class="filtreEcho">
                  <?php
                      if (isset($_POST['geekfilter']) && ($_POST['geekfilter'] == 1 || $_POST['geekfilter'] == 0)) {
                         dataFilter($_POST['geekfilter']);
                      } else {
                          dataFilterAll();
                      }
                  ?>
                </div>

              </form>
						</aside>
					</section>

				</div>
			</main>

      <div class="overlay" id="contact-us">

          <?php
            if (isset($_SESSION["pseudonyme"])){
                include("PHP/includes/genere-profil-perso.php");
                if($_SESSION["pseudonyme"] == "gandalf") {
                $inputAction = "disabled";
        }
            }

          ?>

        <form class="form-lb2" method="post" action="main-page.php?pseudonyme=<?php echo $_SESSION["pseudonyme"] ?>">

          <ul class="form-ul">

            <fieldset>

                <legend> Informations personelles </legend>

            <li>
              <p>Nom</p>
              <input type="text" name="firstname" value="<?php echo $firstname; ?>" <?php echo $inputAction; ?> maxlength="25" required>
            </li>

            <li>
              <p>Prénom</p>
              <input type="text" name="lastname" value="<?php echo $lastname; ?>" <?php echo $inputAction; ?> maxlength="25" required>
            </li>

            <li>
              <p>Date de naissance</p>
              <input type="date" name="birthday" value="<?php echo $birthday; ?>" <?php echo $inputAction; ?> maxlength="10" >
            </li>

            <li>
              <p>Adresse</p>
              <input type="text" name="adress" value="<?php echo $adress; ?>" <?php echo $inputAction; ?> maxlength="40" >
            </li>

            <li>
              <p>Téléphone</p>
              <input type="text" name="phoneNumber" value="<?php echo $phoneNumber; ?>" <?php echo $inputAction; ?> maxlength="25" >
            </li>

            <li>
              <p>Âge</p>
              <input type="number" name="age" value="<?php if(isset($data['naissance'])) {
                                                                echo $age;
                                                            }?>" disabled maxlength="3" >
            </li>

            <li>
              <p>Sexe</p>
                <p>Femme</p>

                <input type="radio" name="sexe" value="femme" <?php echo $inputAction." ".$femaleChecked; ?> >

                <p>Homme</p>

                <input type="radio" name="sexe" value="homme" <?php echo $inputAction." ".$maleChecked; ?> >
            </li>

            <li>
              <p>Êtes-vous un(e) geek ?</p>
              <p>OUI</p>

              <input type="radio" name="geek" value="1" <?php echo $inputAction ." ". $geekYesChecked; ?> >

              <p>NON</p>

              <input type="radio" name="geek" value="0" <?php echo $inputAction ." ". $geekNoChecked; ?> >
            </li>

            </fieldset>
          </ul>

              <ul class="form-ul">

                <fieldset>

                  <legend> Informations complémentaires</legend>

            <li>
              <p>Distribution Linux</p>
              	<?php listselected($options); ?>
            </li>

            <li>
              <p>Site internet n°1</p>
              <input type="text" name="site1" value="<?php echo $site1; ?>" <?php echo $inputAction; ?> maxlength="30" >
            </li>

            <li>
              <p>Site internet n°2</p>
              <input type="text" name="site2" value="<?php echo $site2; ?>" <?php echo $inputAction; ?> maxlength="30" >
            </li>

						<li>
              <p>Langage de programmation n°1</p>
                <?php
                    $inputSelected1 = NULL;
                    listselected2("language1", $inputSelected1, $data);
                ?>
            </li>

            <li>
              <p>Langage de programmation n°2</p>
              <?php
                $inputSelected2 = NULL;
                listselected2("language2", $inputSelected2, $data);
                ?>
            </li>

            <li>
              <p>Langage de programmation n°3</p>
              <?php
                $inputSelected3 = NULL;
                listselected2("language3", $inputSelected3, $data);
                ?>
            </li>

            <li>
              <p>Hobbies n°1</p>
              <input type="text" name="hobbie1" value="<?php echo $hobbie1; ?>" <?php echo $inputAction; ?> maxlength="20" >
            </li>

            <li>
              <p>Hobbies n°2</p>
              <input type="text" name="hobbie2" value="<?php echo $hobbie2; ?>" <?php echo $inputAction; ?> maxlength="20" >
            </li>

            <li>
              <p>Hobbies n°3</p>
              <input type="text" name="hobbie3" value="<?php echo $hobbie3; ?>" <?php echo $inputAction; ?> maxlength="20" >
            </li>
          </fieldset>
        </ul>

        <ul class="form-ul">

          <fieldset>

            <legend> Modification mot de passe </legend>

            <li>
              <p>Pseudonyme</p>
              <input type="text" name="pseudonyme" value="<?php echo $pseudonyme; ?>" disabled>
            </li>

            <li>
              <p>Date d'inscription</p>
              <input type="text" name="register" value="<?php echo $register; ?>" disabled >
            </li>

            <li>
              <input <?php echo $inputAction; ?> type="password" name="oldpassword" placeholder="Ancien mot de passe">
              <input <?php echo $inputAction; ?> type="password" name="newpassword" placeholder="Nouveau mot de passe">
              <input <?php echo $inputAction; ?> type="password" name="checknewpassword" placeholder="Confirmation nouveau mot de passe">
              <button class="form-lb-button-1" type="submit" name="validation">Valider</button>
              <button class="form-lb-button-2" type="submit" name="close">Fermer</button>
            </li>
            </fieldset>
          </ul>

        </form>
      <div class="overlay-close" onclick='location.href="main-page.php?pseudonyme=<?php echo $_GET["pseudonyme"] ?>"'></div>
      </div>
			<footer class="footer-basic-centered">

                    <?php
                        if (!empty($data['stylecolor'])) {
                            echo "<style>header { background-color: " . $data['stylecolor'] . "; opacity: 0.73; } </style>";
                        }
                    ?>

				<p class="footer-company-motto">SearchMe</p>
				<p class="footer-company-name">COPYRIGHT © 2017 SearchMe, TOUS DROITS RÉSERVÉS.</p>


			</footer>
		</body>
		</html>
