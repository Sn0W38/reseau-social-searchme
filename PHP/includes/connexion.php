<?php

    /*  Compare le mdp enregistré dans le fichier txt correspondant au
    *   mail et le mdp en input
    *   Si OK, redirige vers la page générant les profils.
    */

     //(à supprimer si déja include dans la page)

    if (isset($_POST["passwordConnect"])) {

        //$filename = "UserData/" . $_POST["pseudonymeConnect"] . ".txt";
        $filename = $_POST["pseudonymeConnect"];
        $password = md5($_POST["passwordConnect"]);
        $data = dataLecture($filename);
//test
/*
echo "<br> filename : <br>";
var_dump($filename);
echo "<br> data : <br>";
var_dump($data);
echo "<br> mdp rentré : <br>";
var_dump($password);
echo "<br> mdp data :<br>";
var_dump($data["password"]);
echo "<br>";
*/
        if ($data) {

            if ($data["password"] == $password) {

                $_SESSION['img']= $data['photo'];
                header('location: main-page.php?pseudonyme=' . $_POST['pseudonymeConnect'].'&connect=on');

            } else {

                $errorConnect["password"] = "<i class='fa fa-exclamation-circle' aria-hidden='true'> Oups! Il semblerais que le mot de passe ne corresponde pas à ce pseudonyme.</i>";
                $_SESSION['pseudonyme'] = NULL;

            }
            
        } else {
            
            $errorConnect["pseudonyme"] = "<i class='fa fa-exclamation-circle' aria-hidden='true'> Oups! Nous ne connaissons pas ce pseudonyme.</i>";
            
        }
    }
?>
