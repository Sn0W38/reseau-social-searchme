<?php

    /*  Récupére les données enregistrer dans le fichier texte
    *   correspondant au pseudonyme de l'utilisateur
    *   /!\ le mail est récupérer par GET dans l'url. /!\
    *
    *   Si le pseudonyme récupérer dans GET correspond au pseudonyme récupérer dans COOKIES['pseudonyme']
    *   Alors les informations sont modifiables
    */


    $filename = NULL;

    if (isset($_GET["pseudonyme"])) {

        $filename = $_GET["pseudonyme"];

    }
/*
//DEBUG
// echo $filename . "\n";
echo "inpuAction : ".$inputAction."\n";
echo "Cookie :  ".$_COOKIE['pseudonyme']."\n";
//DEBUG
*/

    $data = [ "pseudo" => "",
             "nom" => "",
             "prenom" => "",
             "password" => "",
             "photo" => "",
             "photoMini" => "",
             "sexe" => "",
             "age" => "",
             "naissance" => "",
             "geek" => "",
             "adresse" => "",
             "telephone" => "",
             "email" => "",
             "apropos" => "",
             "distrib" => "",
             "register" => "",
             "site1" => "",
             "site2" => "",
             "site3" => "",
             "language1" => "",
             "language2" => "",
             "language3" => "",
             "hobbie1" => "",
             "hobbie2" => "",
             "hobbie3" => "",
             "stylecolor" => ""
            ];

            $img        = NULL;
            $imgMini    = NULL;
            $pseudonyme = NULL;
            $firstname = NULL;
            $lastname = NULL;
            $sexe = NULL;
            $age = NULL;
            $birthday = NULL;
            $geek = NULL;
            $adress = NULL;
            $phoneNumber = NULL;
            $email = NULL;
            $site1 = NULL;
            $site2 = NULL;
            $language1 = NULL;
            $language2 = NULL;
            $language3 = NULL;
            $hobbie1 = NULL;
            $hobbie2 = NULL;
            $hobbie3 = NULL;
            $about = NULL;
            $distrib = NULL;
            $register = NULL;
            $filename = NULL;

    if (isset($_GET["pseudonyme"])) {

        $filename = $_GET["pseudonyme"];
        $bdd  = new PDO("mysql:host=developont.fr;dbname=developonteur;charset=utf8mb4", "developonteur", "G4rdeP0n!");
        $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $req  = $bdd -> prepare('SELECT pseudo FROM rezo_users WHERE pseudo = :pseudonyme');
        
        
        $reponse = $req -> execute(array('pseudonyme' => $filename));

        // Verifier que la personne existe dans la bdd
        if(!$reponse) {

            echo "<meta http-equiv='refresh' content='1;url=error-page.php'>";//REDIRECTION;

        } else {

            $data = dataLecture($filename);

            if ($data != false) {

                $img        = $data["photo"];
                $pseudonyme = $data["pseudo"];
                $firstname = $data["nom"];
                $lastname = $data["prenom"];
                $sexe = $data["sexe"];
                $birthday = $data["naissance"];
                $age = age($birthday);
                $geek = $data["geek"];
                $adress = $data["adresse"];
                $phoneNumber = $data["telephone"];
                $email = $data["email"];
                $site1 = $data["site1"];
                $site2 = $data["site2"];
                $language1 = $data["language1"];
                $language2 = $data["language2"];
                $language3 = $data["language3"];
                $hobbie1 = $data["hobbie1"];
                $hobbie2 = $data["hobbie2"];
                $hobbie3 = $data["hobbie3"];
                $about = $data["apropos"];
                $distrib = $data["distrib"];
                $register = date("d-m-Y", strtotime($data["register"]));
                $stylecolor = $data["stylecolor"];

            } else {

                echo "<meta http-equiv='refresh' content='1;url=error-page.php'>";//REDIRECTION;

            }

        }

    }
/*
//DEBUG
// echo $filename . "\n";
echo "inpuAction : ".$inputAction."\n";
echo "Cookie :  ".$_SESSION['pseudonyme']."\n";
//DEBUG
*/
?>