<?php
$livreOrLectureError = "";
$livreOrEmptyError = "";
$filename = NULL;
/*$page_part1 = array();
$page_part2 = array();*/

if (isset($_GET["pseudonyme"])) {

    if(!empty($_GET["pseudonyme"])){

        if(isset($_POST['message'])) {

            if(!empty($_POST['message'])) {

                    $pseudonyme = htmlspecialchars($_SESSION['pseudonyme']);
                    $commeenterre = htmlspecialchars($_POST['message']);
                    date_default_timezone_set('UTC');
                    $heure = date("j F Y, H:i:s");

                    $nombre_retour_ligne = 0;
                    $commeenterre = str_split($commeenterre);
                    $taille_com = count($commeenterre);
                    $commentaire_finale = NULL;

                    for ($i=0; $i < $taille_com; $i++)
                    {
                        if ($commeenterre[$i] == "\r" && $nombre_retour_ligne < 2 || $commeenterre[$i] == "\n" && $nombre_retour_ligne < 2)
                        {
                            $commentaire_finale .= $commeenterre[$i];
                            $i++;
                            $nombre_retour_ligne++;
                            if ($commeenterre[($i-1)] != $commeenterre[$i] && $commeenterre[$i] == "\r" || $commeenterre[$i] == "\n")
                            {
                                $commentaire_finale .= $commeenterre[$i];
                            }
                            else
                            {
                                $i--;
                            }
                        }
                        elseif ($commeenterre[$i] != "\r" && $commeenterre[$i] != "\n")
                        {
                            $commentaire_finale .= $commeenterre[$i];
                            $nombre_retour_ligne = 0;
                        }
                    }

                        // on remplace le retour a la ligne par <br>
                    $order = array("\r\n", "\n", "\r");
                    $replace = "<br>";
                        // Traitement des \r\n, ils ne seront pas convertis deux fois.
                    $commentaire_finale = str_replace($order, $replace, $commentaire_finale);

                    /*$page = dataLecture($filename);
                    $table = count($page)/3;*/
                    if (/*!isset($_POST["test_modif"])*/false) {

                        if ($page[("commentaire".($table-1))] != $commentaire_finale) {

                            $page[("pseudonyme".$table)] = $pseudonyme;
                            $page[("heure".$table)] = "</b> - $heure \n\n <p> ";
                            $page[("commentaire".$table)] = $commentaire_finale;
                            //$page = json_encode($page, true);

                            dataEcriture($page, "rewrite", $filename);

                        }
                    }
                    else{
                            dataEcriture($commentaire_finale);

                            $_POST["test_modif"] = 0;
                        }

            } else {

                $livreOrEmptyError = "msg vide";
                //echo "msg vide";

            }
        }
    }
}

?>
