<section class="list">
    <aside>

        <form id="form2" class="form-profil" action="main-page.php?pseudonyme=<?php echo $pseudonyme ?>" method="post">

            <p class="head-second">Choix du template</p>

            <div class="color-check">

                <input type="color" value="<?php echo $data['stylecolor'] ?>" id="toggle0" class="color-input" name="stylecolor" onchange="this.form.submit();">
                <label class="color-label-choice colorLabel" for="toggle0"><i class="fa fa-eyedropper" aria-hidden="true"></i></label>


                <input name="stylecolor" class="color-input" id="toggle1" type="checkbox" value="#13005A" onchange="this.form.submit();">
                <label class="color-label-purple colorLabel" for="toggle1"></label>

                <input name="stylecolor" class="color-input" id="toggle2" type="checkbox" value="#000120" onchange="this.form.submit();">
                <label class="color-label-black colorLabel" for="toggle2"></label>

                <input name="stylecolor" class="color-input" id="toggle3" type="checkbox" value="#920606" onchange="this.form.submit();">
                <label class="color-label-red colorLabel" for="toggle3"></label>

                <input name="stylecolor" class="color-input" id="toggle4" type="checkbox" value="#1E97B5" onchange="this.form.submit();">
                <label class="color-label-blue colorLabel" for="toggle4"></label>

                <input name="stylecolor" class="color-input" id="toggle5" type="checkbox" value="#719A21" onchange="this.form.submit();">
                <label class="color-label-green colorLabel" for="toggle5"></label>

            </div>
        </form>
    </aside>
</section>
