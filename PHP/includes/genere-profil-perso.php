<?php

    /*  Récupére les données enregistrer dans le fichier texte
    *   correspondant au pseudonyme de l'utilisateur
    *   /!\ le mail est récupérer par GET dans l'url. /!\
    *
    *   Si le pseudonyme récupérer dans GET correspond au pseudonyme récupérer dans COOKIES['pseudonyme']
    *   Alors les informations sont modifiables
    */

    $profilNotFoundError = "";

    if(isset($_SESSION["pseudonyme"])){

    $filename = $_SESSION["pseudonyme"];
    $filenameConnect = $_SESSION["pseudonyme"];
    $inputAction = "";
    

        
/*
//DEBUG
// echo $filename . "\n";
echo "inpuAction : ".$inputAction."\n";
echo "Cookie :  ".$_SESSION['pseudonyme']."\n";
//DEBUG
*/

    //modifProfil($_GET['pseudonyme']);

   $data = [ "pseudonyme" => "",
         "firstname" => "",
         "lastname" => "",
         "password" => "",
         "img" => "",
         "imgMini" => "",
         "sexe" => "",
         "age" => "",
         "birthday" => "",
         "geek" => "",
         "adress" => "",
         "phoneNumber" => "",
         "email" => "",
         "about" => "",
         "distrib" => "",
         "register" => "",
         "site1" => "",
         "site2" => "",
         "language1" => "",
         "language2" => "",
         "language3" => "",
         "hobbie1" => "",
         "hobbie2" => "",
         "hobbie3" => ""
        ];

    $data = dataLecture($filename);
    $dataConnect = dataLecture($filenameConnect);
        
    if ($dataConnect != false) {
        $aboutConnect = $dataConnect['apropos'];
        $imgConnect = $dataConnect['photo'];
        $pseudonymeConnect = $dataConnect['pseudo'];
    }

    if ($data != false) {
        
        $pseudonyme = $data["pseudo"];
        $firstname = $data["nom"];
        $lastname = $data["prenom"];
        $password = $data["password"];
        $img = $data["photo"];
        $imgMini = $data["photoMini"];
        $sexe = $data["sexe"];
        $birthday = $data["naissance"];
        $age = age($birthday);
        $geek = $data["geek"];
        $adress = $data["adresse"];
        $phoneNumber = $data["telephone"];
        $email = $data["email"];
        $about = $data["apropos"];
        $distrib = $data["distrib"];
        $register = date("d-m-Y", strtotime($data["register"]));
        $site1 = $data["site1"];
        $site2 = $data["site2"];
        $language1 = $data["language1"];
        $language2 = $data["language2"];
        $language3 = $data["language3"];
        $hobbie1 = $data["hobbie1"];
        $hobbie2 = $data["hobbie2"];
        $hobbie3 = $data["hobbie3"];
        
        $maleChecked = "";
        $femaleChecked = "";

        if ($data['sexe'] == "homme"){

            $maleChecked = "checked";
            $femaleChecked = "";

        } elseif($data['sexe'] == "femme") {

            $maleChecked = "";
            $femaleChecked = "checked";

        } else {

            $maleChecked = "";
            $femaleChecked = "";

        }

        if ($data['geek'] == 1){

            $geekYesChecked = "checked";
            $geekNoChecked = "";

        } elseif($data['geek'] == 0) {

            $geekYesChecked = "";
            $geekNoChecked = "checked";

        } else {

            $geekYesChecked = "";
            $geekNoChecked = "";

        }

    } else {

        $profilNotFoundError = true;
        //echo ":'( nous n'avons pas trouvé ce profils!";

    }

   /** var_dump($_POST);
    echo "\n";
    var_dump($data);**/

}

?>