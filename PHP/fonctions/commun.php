<?php

    session_start();
    require_once("PHP/fonctions/read-write.php"); //Lecture écriture des données
    require_once("PHP/fonctions/filtre-utilisateur.php"); //filtre et affichage de la liste des utilisateur
    require_once("PHP/fonctions/inscription.php"); //création du fichier utilisateur
    require_once("PHP/fonctions/modif-profil.php"); // modification du fichier utilisateur
    require_once("PHP/fonctions/resize-img.php"); // redimenssion de l'image
    require_once("PHP/fonctions/listselected-distrib.php");
    require_once("PHP/fonctions/listselected-lang.php");
    require_once("PHP/fonctions/limit-number-message.php"); // Limite le nombre de commentaires par pages
    /*
    *   Verifie la syntaxe d'un email
    */

    function isEmail($email) {

        $value = preg_match('/^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9_](?:[a-zA-Z0-9_\-](?!\.)){0,61}[a-zA-Z0-9_-]?\.)+[a-zA-Z0-9_](?:[a-zA-Z0-9_\-](?!$)){0,61}[a-zA-Z0-9_]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/', $email);

        return (($value === 0) || ($value === false)) ? false : true;

    }


    function isPseudo($pseudonyme) {

        $value = preg_match('[\w]', $pseudonyme);

        return (($value === 0) || ($value === false)) ? false : true;

    }

    function Age($date_naissance) {
        date_default_timezone_set("UTC");
        $am = explode('-', $date_naissance); //YY MM DD
        $an = explode('/', date('Y/m/d'));
        if(($am[1] < $an[1]) || (($am[1] == $an[1]) && ($am[2] <= $an[2]))) return $an[0] - $am[0];
        return $an[0] - $am[0] - 1;
    }

?>
