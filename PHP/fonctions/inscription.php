<?php

    /* inscription      : récupére les input, les enregistre dans un tableau et
    *                     l'écrit dans un fichier txt
    * @param $post      : $_POST du formulaire
    *
    */

    function inscription($post) {

        date_default_timezone_set('UTC');

        $checkPseudonyme= false;
        $checkLastname  = false;
        $checkFirstname = false;
        $checkEmail     = false;
        $checkPassword  = false;
        $checkGeek      = false;
        $checkCGU       = false;

        $pseudonymeError        = "";
        $psswdError             = "";
        $lastnameError          = "";
        $firstnameError         = "";
        $mailError              = "";
        $geekError              = "";
        $CGUError               = "";
        $inscriptionvalidation  = "";

        if (isset($post['email'])) {

        $bdd  = new PDO("mysql:host=developont.fr;dbname=developonteur;charset=utf8mb4", "developonteur", "G4rdeP0n!");
        $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $req = $bdd->prepare('SELECT email FROM rezo_users WHERE email = :email') or die(print_r($bdd->errorInfo()));
        $checkEmailUsed = $req->execute(array('email' => $post['email']));
        $checkEmailUsed = $req->fetch();
        $req->closeCursor();
//test
/*
echo "<br>Check email Used : <br>";
var_dump($checkEmailUsed);
echo "<br> table_filter: <br>";
var_dump($table_filter);
echo "<br> table_user :<br>";
var_dump($table_user);
*/
        }

        if (isset($post['pseudonyme'])) {

        $bdd  = new PDO("mysql:host=developont.fr;dbname=developonteur;charset=utf8mb4", "developonteur", "G4rdeP0n!");
        $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $req = $bdd->prepare('SELECT pseudo FROM rezo_users WHERE pseudo = :pseudonyme') or die(print_r($bdd->errorInfo()));
        $checkPseudoUsed = $req->execute(array('pseudonyme' => $post['pseudonyme']));
        $checkPseudoUsed = $req->fetch();
        $req->closeCursor();
//test
/*
echo "<br>Check email Used : <br>".$checkPseudoUsed."<br>";
*/
        }

        $data = [ "pseudo" => "",
                 "nom" => "",
                 "prenom" => "",
                 "password" => "",
                 "photo" => "",
                 "photoMini" => "",
                 "sexe" => "",
                 "age" => "",
                 "naissance" => "",
                 "geek" => "",
                 "adresse" => "",
                 "telephone" => "",
                 "email" => "",
                 "apropos" => "",
                 "distrib" => "",
                 "register" => "",
                 "site1" => "",
                 "site2" => "",
                 "language1" => "",
                 "language2" => "",
                 "language3" => "",
                 "hobbie1" => "",
                 "hobbie2" => "",
                 "hobbie3" => ""];

   		    $photo1 = "img/Avatar/01.png";
			$photo2 = "img/Avatar/02.png";
			$photo3 = "img/Avatar/03.png";
			$photo4 = "img/Avatar/04.png";
			$photo5 = "img/Avatar/05.png";
			$photo6 = "img/Avatar/06.png";
			$photo7 = "img/Avatar/07.png";
			$photo8 = "img/Avatar/08.png";
			$photo9 = "img/Avatar/09.png";
			$photo10 = "img/Avatar/10.png";

			$img_default = array($photo1, $photo2, $photo3, $photo4, $photo5, $photo6, $photo7, $photo8, $photo9, $photo10,);

			$rand_img = rand(0, 9);


        if (isset($post["pseudonyme"])) {

            if (!empty($post["pseudonyme"])) {

                if (isPseudo($_POST['pseudonyme'])) {

                     if ($checkPseudoUsed) {
var_dump($checkPseudoUsed);                         
                            $pseudonymeError = "pseudo déja utilisé";

                        } else {

                            $data["pseudo"] = htmlspecialchars($post["pseudonyme"]);
                            $checkPseudonyme = true;

                        }

                } else {

                    $pseudonymeError = "<i class='fa fa-exclamation-circle' aria-hidden='true'>caractére interdit (pseudonyme)</i>";

                }

            } else {

                $pseudonymeError = "<i class='fa fa-exclamation-circle' aria-hidden='true'>champ 'pseudonyme' vide</i>";

            }

        }

        if (isset($post["lastname"])) {

            if (!empty($post["lastname"])) {

                if (preg_match("/([A-Za-z])/",$_POST['lastname'])) {

                    $data["prenom"] = htmlspecialchars($post["lastname"]);
                    $checkLastname = true;

                } else {

                    $lastnameError = "<i class='fa fa-exclamation-circle' aria-hidden='true'>caractére interdit (prénom)</i>";

                }

            } else {

                $lastnameError = "<i class='fa fa-exclamation-circle' aria-hidden='true'>champ 'prénom' vide</i>";

            }

        }

        if (isset($post["firstname"])) {

            if (!empty($post["firstname"])) {

                if (preg_match("/([A-Za-z])/",$_POST['firstname'])) {

                    $data["nom"] = htmlspecialchars($post["firstname"]);
                    $checkFirstname = true;

                } else {

                    $firstnameError = "<i class='fa fa-exclamation-circle' aria-hidden='true'>caractére interdit (nom)</i>";

                }

            } else {

                $firstnameError = "<i class='fa fa-exclamation-circle' aria-hidden='true'>champ 'nom' vide</i>";

            }

        }

        if (isset($post["email"])) {

            if (!empty($post["email"])) {

                if (isEmail($_POST['email'])) {

                    if ($post["email"] == $post["email_confirm"]) {

                        if ($checkEmailUsed) {
var_dump($checkEmailUsed);
                            $mailError = "<i class='fa fa-exclamation-circle' aria-hidden='true'>email déja utilisé</i>";

                        } else {

                            $data["email"] = htmlspecialchars($post["email"]);
                            $checkEmail = true;

                        }


                    } else {

                        $mailError = "<i class='fa fa-exclamation-circle' aria-hidden='true'>Vous n'avez pas rentré le même email.</i>";

                    }

                } else {

                    $mailError = "<i class='fa fa-exclamation-circle' aria-hidden='true'>caractére interdit (email)</i>";

                }

            } else {

                $mailError = "<i class='fa fa-exclamation-circle' aria-hidden='true'>champ 'email' vide</i>";

            }

        }

        if (isset($post["password"])) {

            if (!empty($post["password"])) {

                if (preg_match("/([0-9A-Za-z])/",$_POST['password'])) {

                    if ($post["password"] == $post["password_confirm"]) {

                        $data["password"] = md5(htmlspecialchars($post["password"]));
                        $checkPassword = true;

                    } else {

                        $psswdError = "<i class='fa fa-exclamation-circle' aria-hidden='true'>Vous n'avez pas rentré le même mot de passe.</i>";

                    }

                } else {

                    $psswdError = "<i class='fa fa-exclamation-circle' aria-hidden='true'>caractére interdit (mot de passe)</i>";

                }

            } else {

                $psswdError = "<i class='fa fa-exclamation-circle' aria-hidden='true'>champ 'mot de passe' vide</i>";

            }

        }

        if (isset($post["geek"])) {
            if (!empty($post["geek"]) || $post["geek"] == NULL) {
                if ($post["geek"] == "non") {
                    $data["geek"] = "non";
                    $checkGeek = true;
                } else {
                    if ($post["geek"] == "oui") {
                        $data["geek"] = "oui";
                        $checkGeek = true;
                    }
                }

        }

        } else {

            $geekError = "Etes vous geek?";

        }

        if (isset($post["CGU"])) {

            if (!empty($post["CGU"])) {
                    if ($post["CGU"] == "on") {
                        $checkCGU = true;
                    }
                    else {
                        $CGUError = "<i class='fa fa-exclamation-circle' aria-hidden='true'>Veuillez accepter les conditions</i>";
                    }
                } else {

                    $CGUError = "<i class='fa fa-exclamation-circle' aria-hidden='true'>Veuillez accepter les conditions</i>";

                }

        }

        // vérifie si tous les champs obligatoires ont été rempli
        if ($checkLastname && $checkFirstname && $checkEmail && $checkPassword && $checkGeek && $checkPseudonyme && $checkCGU) {

            $data['register'] = date("d-m-Y");
            $data['photo'] = $img_default[$rand_img];
            $data['photoMini'] = $img_default[$rand_img];
            register($data);
            $inscriptionvalidation = "validé !";

            header('location: main-page.php?pseudonyme=' . $data["pseudo"]);

        } else {

/*
//test
echo "<br>pseudo :" . $checkPseudonyme;
echo "<br>lastname :" . $checkLastname;
echo "<br>firstname :" . $checkFirstname;
echo "<br>mail :" . $checkEmail;
echo "<br>geek :" . $checkGeek;
echo "<br>mdp :" . $checkPassword;
echo "<br>CGU :" . $checkCGU;
echo "<br>mregister :" . $data['register'];


            echo "\nerreur d'écriture";*/


        }

        //echo les erreurs



        $errorMsg = [
                    "pseudonyme" => $pseudonymeError,
                    "password" => $psswdError,
                    "lastname" => $lastnameError,
                    "firstname" => $firstnameError,
                    "mmail" => $mailError,
                    "CGU" => $CGUError ];


        return $errorMsg;

    }
?>
