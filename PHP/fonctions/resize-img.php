<?php

/* resizeImg            : Redimenssionne une image, découpe en carré et enregistre sous deux formats 
 * @param $data         : tableau des infos utilisateur
 * @param $size         : taille en pixel de l'image voulu
 * @param $sizeMini     : taille en pixel de la deuxiéme image voulu
 */

function resizeImg($data, $size, $sizeMini) {
    
            $imgError= NULL;
            $maxsize = 	2097152;
            $extensionvalides = array('jpg', 'jpeg', 'png');
            if($_FILES['img']['size'] <= $maxsize) {

                $extensionupload = strtolower(substr(strrchr($_FILES['img']['name'], '.'), 1));
                if(in_array($extensionupload, $extensionvalides)) {

                    //On récupére les dimensions de l'image

                    $chemin = "UserData/img/".$data["pseudo"].".".$extensionupload;
                    $cheminMini = "UserData/img/".$data["pseudo"]."Mini.".$extensionupload;
                    $result = move_uploaded_file($_FILES['img']['tmp_name'], $chemin);
                    copy ($chemin, $cheminMini);

                    $data['photo'] = $chemin;
//test
/*                    
var_dump($data[img]);                    
*/
                    $data['photoMini'] = $cheminMini;

                    $dimensions = getimagesize($data['photo']);
                    $width_orig = $dimensions[0];
                    $height_orig = $dimensions[1];
/*
//test
echo "\n";
//var_dump($width_orig);
//var_dump($height_orig);
echo "data img: <br>";
var_dump($data['photo']);
var_dump($data['photoMini']);
echo "\n";
//test
*/


                    //On vérifie si les dimensions sont inférieur a $sizeMini
                    if($width_orig>$sizeMini && $height_orig >$sizeMini) {

                        $filename = $data['photo'];
                        $filenameMini = $data['photoMini'];



                        $width = $size;
                        $height= $size;

                        $widthMini = $sizeMini;
                        $heightMini= $sizeMini;
                        $dst_x = 0;
                        $dst_y = 0;
                        $src_x= 0;
                        $src_y= 0;
                        $dst_xMini = 0;
                        $dst_yMini = 0;

                        $ratio_orig = $width_orig/$height_orig;
                        
                        if ($extensionupload == "jpeg" || "jpg") {
                            $src = imagecreatefromjpeg($filename);
                        } elseif ($extensionupload == "png") {
                            $src = imagecreatefrompng($filename);
                        }
                        
                        if ($ratio_orig > 0) {

                            $height= $height_orig;
                            $width= $height;


                            $src_x = ($width_orig-$width)/2;
                            $dest = imagecreatetruecolor($height, $width);
                            imagecopy($dest, $src, 0, 0, $src_x, $src_y, $width, $height);
                            
                            if ($extensionupload == "jpeg" || "jpg") {
                                imagejpeg($dest, $filename);
                            } elseif ($extensionupload == "png") {
                                imagepng($dest, $filename);
                            }   
                            
                            $height_orig = $height;
                            $width_orig = $width;
                            $height = $size;
                            $width = $size;
                            
                            $heightMini= $sizeMini;
                            $widthMini= $sizeMini;
                            
                        } elseif ($ratio_orig < 0) {
                            
                            $width= $width_orig;
                            $height= $width;
                            
                            $src_y = ($height_orig-$height)/2;
                            $dest = imagecreatetruecolor($height, $width);
                            imagecopy($dest, $src, 0, 0, $src_x, $src_y, $width, $height);
                            
                            if ($extensionupload == "jpeg" || "jpg") {
                                imagejpeg($dest, $filename);
                            } elseif ($extensionupload == "png") {
                                imagepng($dest, $filename);
                            }   
                            
                            $height_orig = $height;
                            $width_orig = $width;
                            $height = $size;
                            $width = $size;
                            
                            $widthMini= $sizeMini;
                            $heightMini= $sizeMini;
                            
                        } else {
                            $height= $size;
                            $width= $size;
                            $heightMini= $sizeMini;
                            $widthMini= $sizeMini;
                        }


                        //REDIMENSIONNEMENT 
                        $image_p = imagecreatetruecolor($size, $size);
                        $image_pMini = imagecreatetruecolor($sizeMini, $sizeMini);
                        if ($extensionupload == "jpeg" || "jpg") {
                            $image = imagecreatefromjpeg($filename);
                            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $size, $size, $width_orig, $height_orig);
                            imagejpeg($image_p, $filename);
//var_dump($width_orig);
                            $imageMini = imagecreatefromjpeg($filename);
                            imagecopyresampled($image_pMini, $imageMini, 0, 0, 0, 0, $sizeMini, $sizeMini, $size, $size);
                            imagejpeg($image_pMini, $filenameMini);
                            
                        } elseif ($extensionupload == "png") {
                            $image = imagecreatefrompng($filename);
                            imagecopyresampled($image_p, $image, $dst_x, $dst_y, 0, 0, $width, $height, $width_orig, $height_orig);
                            imagejpng($image_p, $filename);
                            
                            $imageMini = imagecreatefrompng($filenameMini);
                            imagecopyresampled($image_pMini, $imageMini, 0, 0, 0, 0, $widthMini, $heightMini, $width_orig, $height_orig);
                            imagejpng($image_pMini, $filenameMini);
                        }

/*test
echo "\n";
var_dump($image_p);
var_dump($image);
echo "\n";
test*/
                        

/*
//test
echo "\n";
var_dump($image_pMini);
var_dump($imageMini);
echo "\n";
//test
*/

                } else {

                    $imgError = "Votre photo de profil dois etre en format jpg, jpeg, ou png";
                    
                }

            } else {

                $imgError = "Votre photo de profil dois etre en format jpg, jpeg, ou png";

            }
                
        }
    
        return $data;
    
    }

?>