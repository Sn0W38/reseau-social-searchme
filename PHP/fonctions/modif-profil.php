<?php

    /* modifProfil          : Récupére les data d'un profil, et modifie les champs remplis dans le fichier UserData
    *
    * @param $user string   : Pseudonyme de l'utilisateur (récupérer en $_GET)
    *
    */

    /*
    *
    *  /!\ EN COURS DECRITURE  /!\
    *
    */

    function modifProfil($user) {

        $filename   = $user;
        $data       = dataLecture($filename);

        $pseudonymeError= "";
        $psswdError     = "";
        $lastnameError  = "";
        $firstnameError = "";
        $mailError      = "";
        $geekError      = "";
        $imgError       = "";
        $errorMsg       = NULL;

        //PSEUDONYME INCHANGABLE

        if (isset($_POST["lastname"])) {

            if (!empty($_POST["lastname"])) {

                if (preg_match("/([A-Za-z])/",$_POST['lastname'])) {

                    $data["prenom"] = htmlspecialchars($_POST["lastname"]);
                    $checkLastname = true;

                } else {

                    $lastnameError = "caractére interdit (Nom)";

                }

            }

        }

        if (isset($_POST["firstname"])) {

            if (!empty($_POST["firstname"])) {

                if (preg_match("/([A-Za-z])/",$_POST['firstname'])) {

                    $data["nom"] = htmlspecialchars($_POST["firstname"]);
                    $checkFirstname = true;

                } else {

                    $errorMsg["firstname"] = "caractére interdit (Prénom)";

                }

            } else {

                $errorMsg["firstname"] = "champ 'firstnom' vide";

            }

        }

        if (isset($_POST["email"])) {

            if (!empty($_POST["email"])) {

                if (isEmail($_POST['email'])) {

                        $data["email"] = htmlspecialchars($_POST["email"]);
                        $checkEmail = true;

                } else {

                    $errorMsg["email"] = "caractére interdit (email)";

                }

            } else {

                $errorMsg["email"] = "champ 'email' vide";

            }

        }

        if (isset($_POST["newpassword"])) {

            if (!empty($_POST["newpassword"])) {
                
                if (md5($_POST["oldpassword"]) == $data["password"]) {

                    if (preg_match("/([0-9A-Za-z])/",$_POST['newpassword'])) {

                        if ($_POST["newpassword"] == $_POST["checknewpassword"]) {

                            $data["password"] = md5(htmlspecialchars($_POST["newpassword"]));
                            $data['checkPassword'] = true;
                            $errorMsg["password"] = "victoire!";

                        } else {

                            $errorMsg["password"] = "Vous n'avez pas rentré le même mot de passe.";

                        }

                    } else {

                        $errorMsg["password"] = "caractére interdit (password)";

                    }

                } else {
                    $errorMsg["password"] = "l'ancien mot de passe ne correspond pas";
                }
                
            }


        }

        if (isset($_POST["geek"])) {


            if (!empty($_POST["geek"])) {
                
                        //$data["geek"] = htmlspecialchars($_POST["geek"]);
                        $data["geek"] = $_POST["geek"];
                        $checkGeek = true;    

            } else {

                $errorMsg["geek"] = "Etes vous geek?";

            }

        }

         if (isset($_POST["sexe"])) {


            if (!empty($_POST["sexe"])) {

                        $data["sexe"] = htmlspecialchars($_POST["sexe"]);

            }

         }

        if (isset($_POST["age"])) {


            if (!empty($_POST["age"])) {

                        $data["age"] = htmlspecialchars($_POST["age"]);

            }

        }

        if (isset($_POST["birthday"])) {


            if (!empty($_POST["birthday"])) {

                        $data["naissance"] = htmlspecialchars($_POST["birthday"]);

            }

        }
        
        if (isset($_POST["adress"])) {


            if (!empty($_POST["adress"])) {

                        $data["adresse"] = htmlspecialchars($_POST["adress"]);

            }

        }

        if (isset($_POST["phoneNumber"])) {


            if (!empty($_POST["phoneNumber"])) {

                        $data["telephone"] = htmlspecialchars($_POST["phoneNumber"]);

            }

        }

        if (isset($_POST["about"])) {


            if (!empty($_POST["about"])) {

                        $data["apropos"] = htmlspecialchars($_POST["about"]);

            }

        }

        if (isset($_POST["distrib"])) {


            if (!empty($_POST["distrib"])) {

                        $data["distrib"] = htmlspecialchars($_POST["distrib"]);

            }

        }

        if (isset($_POST["register"])) {


            if (!empty($_POST["register"])) {

                        $data["register"] = htmlspecialchars($_POST["register"]);

            }

        }

        if (isset($_POST["site1"])) {


            if (!empty($_POST["site1"])) {

                        $data["site1"] = htmlspecialchars($_POST["site1"]);

            }

        }

        if (isset($_POST["site2"])) {


            if (!empty($_POST["site2"])) {

                        $data["site2"] = htmlspecialchars($_POST["site2"]);

            }

        }

        if (isset($_POST["site3"])) {


            if (!empty($_POST["site3"])) {

                        $data["site3"] = htmlspecialchars($_POST["site3"]);

            }

        }

        if (isset($_POST["language1"])) {


            if (!empty($_POST["language1"])) {

                        $data["language1"] = htmlspecialchars($_POST["language1"]);

            }

        }

        if (isset($_POST["language2"])) {


            if (!empty($_POST["language2"])) {

                        $data["language2"] = htmlspecialchars($_POST["language2"]);

            }

        }

        if (isset($_POST["language3"])) {


            if (!empty($_POST["language3"])) {

                        $data["language3"] = htmlspecialchars($_POST["language3"]);

            }

        }

        if (isset($_POST["hobbie1"])) {


            if (!empty($_POST["hobbie1"])) {

                        $data["hobbie1"] = htmlspecialchars($_POST["hobbie1"]);

            }

        }

        if (isset($_POST["hobbie2"])) {


            if (!empty($_POST["hobbie2"])) {

                        $data["hobbie2"] = htmlspecialchars($_POST["hobbie2"]);

            }

        }

        if (isset($_POST["hobbie3"])) {


            if (!empty($_POST["hobbie3"])) {

                        $data["hobbie3"] = htmlspecialchars($_POST["hobbie3"]);

            }

        }

        if (isset($_POST["stylecolor"])) {


            if (!empty($_POST["stylecolor"]) && $_POST["stylecolor"] != NULL && $_POST["stylecolor"] != "") {

                        $data["stylecolor"] = htmlspecialchars($_POST["stylecolor"]);

            } 

        }

        if(isset($_FILES['img']) AND (!empty($_FILES['img']['name']))) {

            $data = resizeImg($data, 125, 25);
//test

	   }
        updateProfil($data);

        /*

        } else {

             test
            echo "\npseudo :" . $checkPseudonyme;
            echo "\nlastname :" . $checkLastname;
            echo "\nfirstname :" . $checkFirstname;
            echo "\nmail :" . $checkEmail;
            echo "\ngeek :" . $checkGeek;
            echo "\nmdp :" . $checkPassword;

            echo "\nerreur d'écriture";

        }

        */

        //echo les erreurs
        /*
        echo $pseudonymeError . "\n";
        echo $psswdError . "\n";
        echo $lastnameError . "\n";
        echo $firstnameError . "\n";
        echo $mailError . "\n";
        */
        return $data;
        
    }
/*
//TEST
var_dump($data);
*/

?>
