<?php

    include("PHP/fonctions/commun.php");

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Erreur 404</title>
        <link rel="stylesheet" type="text/css" href="css/style-error-page.css">
    </head>

    <body>

    <header>
        <h1>Désolé</h1>
    </header>

    <main>
      <p>Cette page n'existe pas, tu n'existe pas, rien n'est vrai.</p>
      <p>Tu es dans la matrice.</p>
      <p><a href="index.php">revenir à la page d'acceuil</a></p>
    </main>

    </body>
</html>
