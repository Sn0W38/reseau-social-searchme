# Réseau social 

## Table n°4

*Myléne, Quentin, Kevin, Stephane, Joffrey*

* Responsable commercial : **Kevin**
* Responsable technique : **Quentin**
* Scrum Master : **Joffrey**

## Description : 
Un réseau social permettant à chaque utilisateur d'enregistrer son profil, 
consulter la liste des profils enregistrés et de modifier son profil à condition d'être connecté

## Organisation des pages

#### Accueil

Page d'accueil du site

Contenu :

* Inscription
 *- crée un nouveau profil*

* Connexion
 *- se connecte avec un profil existant*
 
#### Mur

Page principal du site

Contenu :

* Profil
 *- résumé du profil d'un l'utilisateur* 
* Livre d'or
 *- correspondant au profil de la page*
* Liste des inscrits
 *- liste de tous les inscrits, avec possibilité de filtrer*

#### Profil

Page de profil détaillé

contenu : 

* header
 *- infos principales (nom prenom pseudo photo...)*
* Liste des infos
 *- modifiable si l'utilisateur connécté correspont à la page*

## Organisation des fichiers

* index.php *-page d'acceuil*
* mainIndex.php *- page principal*
* profil.php *- profildétaillé*
* /Documentation
* /img
* /assets
* /PHP
    * /include
    * /fonctions
* /CSS
* /UserData

## UserData

Les fichiers contenant les informations utilisateur sont enregistré dans le dossier /UserData sous la forme [pseudo].txt

Elles sont encodé en json au moment de l'écriture

    json_encode($data)    
    
Elles sont décodé sous forme de tableau

    json_decode($data, true)    

Dans $data, les clés utilisés sont :

    $data["pseudo"]
    $data["prenom"]
    $data['nom']
    $data['email']
    $data['password']
    $data['geek']
    $data['photo']
    $data['photoMini']
    $data['sexe']
    $data['age']
    $data['naissance']
    $data['geek']
    $data['adresse']
    $data['telephone']
    $data['email']
    $data['apropos']
    $data['distrib']
    $data['register']
    $data['site1']
    $data['site1']
    $data['site2']
    $data['language1']
    $data['language2']
    $data['language3']
    $data['hobbie1']
    $data['hobbie2']
    $data['hobbie3']
    
### Traitement des images de profils

lors de l'upload de l'image, les images sont redimensionner sous deux formats de taille:

125*125 *-Image de Profil*

25*25 *-Miniature de l'image, pour la liste*

Cette fonctionnalité **requiert l'instalation de la librairie php GD.**

## Fonctions

### readWrite.php

#### dataEcriture($data, $filename) :
    dataEcriture         : ecrit dans un fichier txt
    @param    $data      : données utilisateurs
    @param    $filename  : fichier dans lequel on écrit
    @return   $fileExist : retourne true ou false
    @fonction json_encode: encode une liste 
    @fonction json_decode: decode une liste
    
#### dataLecture($filename) :
    dataLecture           : ecrit dans un fichier txt
    @param    $filename   : fichier dans lequel on écrit
    @return   $fileExist  : retourne true ou false
    @return   $data       : retourne une variable contenant le contenu du 									fichier txt   
    
### filtre_utilisateur.php

#### user_list($dossier) :
    user_list               : recupére tout les fichier dans un dossier
    @param $dossier         : emplacement du dossier ou il y a les fichiers User
    @return $user_checked   : retourne un tableau des données User
    
#### filter_user($table_filter , $table_user)
    
### inscription.php

#### inscription($post)
    inscription     : récupére les input, les enregistre dans un tableau et l'écrit dans un fichier txt
    @param $post    : $_POST du formulaire
    @return  $data  : tableau mis à jour

### commun.php

#### isEmail($email) :
    isEmail         : vérifie la syntaxe d'un email
    @param $email   : string contenant l'adresse mail
    @return $value  : bool
    
## Notes ponctuelles :

###Droits d'écriture

Les droits d'écriture doivent étre donnés à apache sur :

* /UserData
* /LivreDor

## Consignes techniques :

### INDEX - filtre



**1-1 - readWrite.php :**

Chaque fonctionnalités sera codé dans un fichier php différents. 
Si des fonctions sont utilisés dans plusieurs fonctionnalités, elles devront être mis dans un autre fichier commun.php. 
Toutes les opérations de lectures et d’écritures dans les fichiers seront réalisées dans un unique fichier php readWrite.php

Pour l’encodage des fichiers de l’utilisateur, nous utilisons le fonction JSON.
Une utilisation courante de JSON est de lire les données à partir d'un serveur Web, et afficher les données dans une page Web.
Il permet de rendre une ligne de donnée d’un tableau PHP dans un langage que le PHP reconnaît et comprend afin de stocker les données dans un simple fichier TXT.

Par exemple : 

{"pseudonyme":"joffreyal","firstname":"alcaraz","lastname":"joffrey"}

On peut encoder mais aussi décoder grace a JSON, ce qui permettra de faire un simple tableau PHP lisible.

Par exemple :

array([pseudonyme] = "joffreyal", [firstname] = "alcaraz", [lastname]:"joffrey")



**1-2 - Utilisateur :**

Les données de Profil seront stockées dans un ou plusieurs fichiers nommé avec l’email de l’utilisateur

**1-3 - Indentation :**

Exemples de la structure d'un fichier CSS :

    body {
    font-family: Arial, sans-serif;
    font-size: 16px;
    text-align: left;
    }

**1-4 - Documentation :**

Une note technique est a rédigé concernant le détail des différents appels POST ou GET et les paramètre mis en jeu. Elle est a remettre à son responsible technique à Début du Projet + 4 jours
Une note technique est a rédigé concernant la gestion des fichiers, leurs organisation et ce qui y est ecrit. Elle est a remettre à son responsible technique à Début du Projet + 10 jours

**1-5 - Générales :**

* Espace après balise, classe, id, propriété, virgule
* Saut de ligne après une accolade fermante
* Attention aux espaces invisibles superflues - les éviter si possible
* Variables nommés en franglais

## Auteurs/Contributeurs :

- Mylène
- Kevin
- Stéphane
- Joffrey
- Quentin


   